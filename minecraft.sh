#!/bin/bash

echo "==========Server Details=========="
echo "Server ID: $(hostname | cut -d '-' -f 1)"
echo "Server RAM: $SERVER_MEMORY MB"
echo "Server IP: $SERVER_IP"
echo "Server Port: $SERVER_PORT"
echo "==========Server Details=========="
echo "eula=true" > eula.txt 

if [ -e "server.jar" ]
then 
    JAR="server.jar"
    echo "Found "$JAR
    minimumsize=100000
    actualsize=$(wc -c <"$JAR")
    if [ $actualsize -lt $minimumsize ]
    then
        echo $JAR" is not a valid size"
		rm "$JAR"
        JAR=""
    fi
else
    JAR=""
    echo "Could not find server.jar"
fi

if [ "$JAR" = "" ]
then
    echo "Please type the number (and click enter), which server software and version you want to download."
    PS3='(1-34): '
    options=("Paper 1.15 |" "Paper 1.14.4 |" "Paper 1.13.2 |" "Paper 1.12.2 |" "Paper 1.11.2 |" "Paper 1.10.2 |" "Paper 1.9.4 |" "Paper 1.8.x |" "Paper 1.7.10 |" "Spigot 1.7.10 |" "Spigot 1.8.8 |" "Spigot 1.9.4 |" "Spigot 1.10.2 |" "Spigot 1.11.2 |" "Spigot 1.12.2 |" "Spigot 1.13.2 |" "Spigot 1.14.4 |" "Spigot 1.15 |" "Spigot 1.15.1 |" "Taco 1.8.8 |" "Taco 1.11.2 |" "Taco 1.12.2 |" "Vanilla 1.7.10 |" "Vanilla 1.8.8 |" "Vanilla 1.9.4 |" "Vanilla 1.10.2 |" "Vanilla 1.11.2 |" "Vanilla 1.12.2 |" "Vanilla 1.13.2 |" "Vanilla 1.14.4 |" "Vanilla 1.15 |" "Vanilla 1.15.1 |" "Nukkit |" "Custom")
    select opt in "${options[@]}"
    do
        case $opt in
            "Paper 1.15 |")
                echo "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper-1.15/lastSuccessfulBuild/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.14.4 |")
                echo "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper-1.14/lastSuccessfulBuild/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.13.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper-1.13/lastSuccessfulBuild/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.12.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/lastSuccessfulBuild/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.11.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/1104/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.10.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/916/artifact/paperclip-916.2.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.9.4 |")
                echo "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/773/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.8.x |")
                echo "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/443/artifact/Paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.7.10 |")
                echo "You selected $REPLY) $opt"
				curl "https://yivesmirror.com/files/paper/PaperSpigot-1.7.10-R0.1-SNAPSHOT-latest.jar" -o server.jar 2> /dev/null > /dev/null
				break        
                ;;
            "Spigot 1.7.10 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.7.10-SNAPSHOT-b1657.jar" -o server.jar 2> /dev/null > /dev/null
				break                
                ;;
            "Spigot 1.8.8 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.8.8-R0.1-SNAPSHOT-latest.jar" -o server.jar 2> /dev/null > /dev/null
				break  
                ;;
            "Spigot 1.9.4 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.9.4-R0.1-SNAPSHOT-latest.jar" -o server.jar 2> /dev/null > /dev/null
				break                       
                ;;
            "Spigot 1.10.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.10.2-R0.1-SNAPSHOT-latest.jar" -o server.jar 2> /dev/null > /dev/null
				break    
                ;;      
            "Spigot 1.11.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.11.2.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;    
            "Spigot 1.12.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.12.2.jar" -o server.jar 2> /dev/null > /dev/null
				break 
                ;;
            "Spigot 1.13.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.13.2.jar" -o server.jar 2> /dev/null > /dev/null
				break 
                ;;
            "Spigot 1.14.4 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.14.4.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;    
            "Spigot 1.15 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.15.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;   
            "Spigot 1.15.1 |")
                echo "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.15.1.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;   
            "Taco 1.8.8 |")
                echo "You selected $REPLY) $opt"
				curl "https://yivesmirror.com/files/tacospigot/TacoSpigot-1.8.8.jar" -o server.jar 2> /dev/null > /dev/null
				break  
                ;;
            "Taco 1.11.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://yivesmirror.com/files/tacospigot/TacoSpigot-1.11.2-b102.jar" -o server.jar 2> /dev/null > /dev/null
				break                     
                ;;        
            "Taco 1.12.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://yivesmirror.com/files/tacospigot/TacoSpigot-1.12.2-b114.jar" -o server.jar 2> /dev/null > /dev/null
				break   
                ;;        
            "Vanilla 1.7.10 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.7.10/server/952438ac4e01b4d115c5fc38f891710c4941df29/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;        
            "Vanilla 1.8.8 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.8.8/server/5fafba3f58c40dc51b5c3ca72a98f62dfdae1db7/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;        
            "Vanilla 1.9.4 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.9.4/server/edbb7b1758af33d365bf835eb9d13de005b1e274/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;      
            "Vanilla 1.10.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.10.2/server/3d501b23df53c548254f5e3f66492d178a48db63/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;        
            "Vanilla 1.11.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.11.2/server/f00c294a1576e03fddcac777c3cf4c7d404c4ba4/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;        
            "Vanilla 1.12.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.12.2/server/886945bfb2b978778c3a0288fd7fab09d315b25f/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;     
            "Vanilla 1.13.2 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/v1/objects/3737db93722a9e39eeada7c27e7aca28b144ffa7/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;   
            "Vanilla 1.14.4 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/v1/objects/d0d0fe2b1dc6ab4c65554cb734270872b72dadd6/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;   
            "Vanilla 1.15 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/v1/objects/e9f105b3c5c7e85c7b445249a93362a22f62442d/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Vanilla 1.15.1 |")
                echo "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/v1/objects/4d1826eebac84847c71a77f9349cc22afd0cf0a1/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;  
            "Nukkit |")
                echo "You selected $REPLY) $opt"
				curl "https://ci.nukkitx.com/job/NukkitX/job/Nukkit/job/master/lastSuccessfulBuild/artifact/target/nukkit-1.0-SNAPSHOT.jar" -o server.jar 2> /dev/null > /dev/null
				break 
                ;;                                         
            "Custom")
                echo "You selected $REPLY) $opt"
				echo "Upload your custom jar via File Management and rename it to server.jar"
				break
                ;;
            *) echo "invalid option $REPLY";;            
        esac
    done
	JAR="server.jar"
fi
echo "Downloaded $JAR"
echo "==========Starting=========="
java -Xms128M -Xmx"$SERVER_MEMORY"M -Dterminal.jline=false -Dterminal.ansi=true -jar "$JAR"
